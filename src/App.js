import React, {Component} from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { connect } from 'react-redux'
import logo from './logo.svg';
import {TopPage} from './topPage.js'
import {PostPage} from './postPage.js'
import './App.css';

class App extends Component {
  render() {
    return(
      <div className="App">
        <img src={logo} className="App-logo" alt="logo" />
        <Router>
          <Switch>
            <Route exact path="/" component={TopPage} />
            <Route path="/post" component={PostPage} />
          </Switch>
        </Router>
      </div>
    )
  }
}


function mapStateToProps(state) {
  return state
}

export default App;
